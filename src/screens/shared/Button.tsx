import styled from "styled-components"

interface IButton {
  size?:number;
  color?: string;
}

const Button = styled.button<IButton>`
  display: inline-block;
  border-radius: 2em;
  padding: ${props => (props.size||2)*0.5}em ${props => (props.size||2)}em;
  color: white;
  background-color: ${props=>props.color};
  border: none;
  box-shadow: 0 5px 3px #0000003f;
  text-decoration: none;
  cursor: pointer;
  float: right;
  margin: ${props => (props.size||2)*0.25}em;
  font-size: ${props => (props.size||2)}em;
  text-transform: uppercase;
  font-family: 'Open Sans';
  font-weight: 600;
`
export default Button