import React from "react";
import Jumbotron from "../../shared/Jumbotron";
import FlexBox from "../../shared/FlexBox";
import Image from "../../shared/Image";
import Heading from "../../shared/Heading";
import Paragraph from "../../shared/Paragraph";
import Container from "../../shared/Container";
import Credits from "../../Portal/Credits";

const flexBoxStyle = {
  flex: '1 1 200px',
  padding: '3em',
  backgroundImage: 'url(/images/bg-why-us.png)',
  backgroundSize: 'contain',
  backgroundRepeat: 'no-repeat'
}

const WhyUs = () => {

  return (
    <Jumbotron>
      <Container>
        <FlexBox row style={{ flexWrap: 'wrap-reverse', margin: '3em' }}>
          <FlexBox style={flexBoxStyle}>
            <Heading size={2}>
              ¿Por qué elegirnos?
            </Heading>
            <Paragraph>
              Somos una empresa que aglomera el know how de multitud de profesionales a lo largo de más de 10 años de trayectoria en impulso de PyMEs con apoyo de instituciones como CAME, FECOBA, ASEMPIO e instituciones públicas. Queremos que tu empresa tenga éxito y te guiamos para que aumentes tus posibilidades al máximo.
            </Paragraph>
          </FlexBox>
          <div style={{ flex: '1 1 200px', position: 'relative' }}>
            <Image style={{ maxWidth: '100%', aspectRatio: '1/1' }} src="/images/man-working.jpg" />
            <Credits>
              Photo by <a href="https://unsplash.com/@cikstefan?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Štefan Štefančík</a> on <a href="https://unsplash.com/s/photos/business-design?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
            </Credits>
          </div>
        </FlexBox>
      </Container>
    </Jumbotron>
  )
}

export default WhyUs