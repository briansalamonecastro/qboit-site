import styled from 'styled-components';

interface IHeading{
  size?: number;
}

const Heading = styled.h1<IHeading>`
  font-size: ${props => props.size === 4? '1em'
    : props.size === 3? '2em'
    : props.size === 2? '3em'
    :'4em'};
  font-weight: bold;
`
export default Heading