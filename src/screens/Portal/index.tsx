import { Link } from "react-router-dom"
import Header from "../shared/Header"
import Heading from "../shared/Heading"
import Splash from "../shared/Splash"
import FlexBox from "../shared/FlexBox"
import styled, {keyframes} from "styled-components"
import Image from "../shared/Image"
import Page from "../shared/Page"
import Credits from "./Credits"

const ThisHeading = styled(Heading)`
  display: inline;
  @media (max-width: 700px){
    font-size: 3em;
  }
`

const BlurredImage = styled(Image)`
  height: 100vh;
  width: 100vw;
  filter: blur(2px);
  position: fixed;
  background-image: radial-gradient(ellipse at bottom, #ffffff9f 20%,#0000004f 80%,  transparent),
    url(/images/edificios.jpg);
  background-size: cover;
`

const wep = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`

const ThisSplash = styled(Splash)`
  margin: auto;
  max-width: 100%;
  animation: ${wep} 1s normal forwards;
`

const ThisPage = styled(Page)`
  display: flex;
  width: 100vw;
  height: 100vh;
`
const ContainerDiv = styled.div`
  position: relative;
  width: 100vw;
  height: 100vh;
`

const Div = styled.div`
  margin: 1em;
  font-family: 'Open Sans';
  font-size: 1.1em;
  opacity: 0;
  animation: ${wep} 0.5s forwards;
`

const Portal = () => (
<ContainerDiv>
  <BlurredImage />
  <ThisPage>
    <ThisSplash>
      <Header>
        <Image style={{ margin: '-2em 0', maxWidth: '100%' }} src='/images/qboit_letras_logo.png' />
        <ThisHeading>Consultora</ThisHeading>
        <Div style={{animationDelay: '0.8s', fontSize: '1.5em' }}>Nuestras secciones:</Div>
      </Header>
      <FlexBox row>
        <Div style={{animationDelay: '1s'}}><Link to="/it">IT Solutions</Link></Div>
        <Div style={{animationDelay: '1.2s'}}><Link to="/emprendedurismo">Entrepreneur Consulting</Link></Div>
        <Div style={{animationDelay: '1.4s'}}><Link to="/negocios">Business Solutions</Link></Div>
      </FlexBox>
    </ThisSplash>
  </ThisPage>
    <Credits>
      Photo by <a href="https://unsplash.com/@seanpollock?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Sean Pollock</a> on <a href="https://unsplash.com/s/photos/enterprise?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
    </Credits>
</ContainerDiv>
)

export default Portal