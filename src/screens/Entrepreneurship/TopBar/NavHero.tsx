import styled from 'styled-components';

const NavHero = styled.div`
  font-size: 1.5em;
  font-weight: 300;
  color: whitesmoke;
  white-space: nowrap;

  @media (max-width: 700px){
    font-size: 1em;
  }
`
export default NavHero
