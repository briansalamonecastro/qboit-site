import React from "react";
import TopBar from "../Business/TopBar";
import Page from "../shared/Page";
import EventInfo from "./EventInfo";


const BusinessInfo = () => (
  <Page style={{backgroundColor: 'whitesmoke'}}>
    <TopBar />
    <EventInfo />
  </Page>
)

export default BusinessInfo