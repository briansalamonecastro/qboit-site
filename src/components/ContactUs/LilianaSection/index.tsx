import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram, faFacebook, faTwitter, faLinkedin } from "@fortawesome/free-brands-svg-icons";
import ImageContainer from "../../../screens/shared/ImageContainer";
import Image from "../../../screens/shared/Image";
import Paragraph from "../../../screens/shared/Paragraph";


const LilianaImageContainer = styled(ImageContainer)`
  margin-right: 4em;
  width: 30%;
  margin-bottom: 0;

  @media (max-width: 700px){
    margin-right: 2em;
    width: 80%;
    margin-bottom: 1em;
  }
`

const LilianaImage = styled(Image)`
  width: 100%;
  border-radius: 100%;

  box-shadow: 0 0 0 1em salmon;
  border: 1em solid white;

  @media (max-width: 700px){
    box-shadow: 0 0 0 0.5em salmon;
    border: 0.5em solid white;
  }
`
const Div = styled.div`
  flex: 1 1;
  padding-left: 1em;
`

const B = styled.b`
  color: white;
  text-shadow: 0 0 3px #000;
  font-size: 1.2em;
`


const SocialIcons = styled.a.attrs(() => ({ target: '_blank' }))`
  @media (max-width: 700px){
    zoom: 0.7;
  }

  & svg {
    transform: scale(1);
  }

  & svg: hover {
    color: #db43db;
    transform: scale(1.2);
    transition: 0.1s ease-out;
  }
`

const Bio = styled.div`
  padding: 0.2em;
  padding-left: 1em;
  margin-left: 5em;
  border-radius: 1em;
  background-color: #ffffff60;

  @media (max-width: 700px){
    padding: unset;
    margin-left: 0;
    background-color: unset;
    color: white;
  } 
`

const I = styled.i`
  color: #111;
  font-weight: bold;
  display: block;
  margin-bottom: 1em;
`

const LilianaSection = () => (
  <Div>
    <LilianaImageContainer>
      <LilianaImage src='/images/liliana_castro.png' />
      <SocialIcons href='https://instagram.com/lilianar_castro'>
        <FontAwesomeIcon icon={faInstagram} fixedWidth size='2x' />
      </SocialIcons>
      <SocialIcons href='https://facebook.com/liliana.castro.3386585'>
        <FontAwesomeIcon icon={faFacebook} fixedWidth size='2x' />
      </SocialIcons>
      <SocialIcons href='https://twitter.com/lilianar_castro'>
        <FontAwesomeIcon icon={faTwitter} fixedWidth size='2x' />
      </SocialIcons>
      <SocialIcons href='https://ar.linkedin.com/in/liliana-rosa-castro-50374635'>
        <FontAwesomeIcon icon={faLinkedin} fixedWidth size='2x' />
      </SocialIcons>
    </LilianaImageContainer>
    <B>Liliana Rosa Castro</B>
    <I>CEO de QBOit</I>
    <Bio>
      <Paragraph>
        Prof. Música, Bachiller Científico, ejerció como Prof. Matemática e Informática. Diplomada en Marketing Digital. Estudio Ing. en Construcciones en UTN. Inteligencia Emocional. Neurociencias aplicadas. Trabajo en equipo. Formadora de Formadores.
      </Paragraph>
      <Paragraph>
        Secretaria Relaciones Institucionales de MECAME, Consejo Directivo de FECOBA, Presidente de ASEMPIO.
      </Paragraph>
    </Bio>
  </Div>
)

export default LilianaSection