import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import OffButton from "./OffButton"

const ReturnButton = styled(OffButton)`
  position: absolute;
  margin-left: -4em;
  margin-top: 80vh;
  margin-bottom: 20vh;

  & p{
    display: none;
  }

  &: hover{
    & p {
      display: inline;
    }
  }
  
  @media (max-width: 950px){
    margin-left: 0em;
    margin-top: 80vh;
  }
`

const Return = () => {
  const goBack = () => window.history.back();

  return (
    <ReturnButton color={'#793702'} size={1} onClick={goBack}>
      <FontAwesomeIcon fixedWidth size='sm' icon={faArrowLeft}/> 
      <p style={{marginLeft: '0.5em'}}>Volver</p>
    </ReturnButton>
  )
}

export default Return