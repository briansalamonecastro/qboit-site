import React from 'react'
import { faCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import ListItem from '../../shared/ListItem'
import styled from 'styled-components'

const StepParagraph = styled.p`
  font-size: 2em;
  font-weight: lighter;
  margin-left: 2em;

  @media (max-width: 700px){
    margin-left: 3em;
    font-size: 1.2em;
  }
`

const Step = ({ color, icon, text }: any) => {
  return (
    <ListItem style={{ margin: '5em 0'}}>
      <span className="fa-layers fa-fw fa-li" >
        <FontAwesomeIcon size="3x" fixedWidth transform='grow-12 down-8' color={color} icon={faCircle} />
        <FontAwesomeIcon size="3x" fixedWidth transform='down-8' icon={icon} />
      </span>
      <StepParagraph>
        {text}
      </StepParagraph>
    </ListItem>
  )
}

export default Step