import React from "react";
import Jumbotron from "../../shared/Jumbotron";
import { faArrowUp, faCogs, faChartLine, faHourglassHalf } from '@fortawesome/free-solid-svg-icons'
import Heading from "../../shared/Heading";
import Step from "./Step";
import Container from "../../shared/Container";
import styled from "styled-components";

const RainbowJumbotron = styled(Jumbotron)`
  background-image: 
  radial-gradient(
    circle at 100% 0, 
    transparent 30%,
    #d15d80 30.1%, 
    #d15d80 32%, 
    transparent 32.1%,
    transparent
  ), radial-gradient(
      circle at 100% 0, 
      #7c4dff, 
      #ee32ee  15%, 
      #db819c 15.1%, 
      #cebac0 60%, 
      #53f4ff 60.5%, 
      #b8c3ff 80%, 
      white
    );
`
const CenteredHeading = styled(Heading)`
 text-align: center;

 @media (max-width: 700px) {
   font-size: 2.5em;
   margin: 2em 0;
 }
`


const HowToStart = () => {

  return (
    <RainbowJumbotron>
      <Container>
        <CenteredHeading>¿Cómo inicio una empresa?</CenteredHeading>
        <ul className='fa-ul'>
          <Step color={'orange'} icon={faArrowUp} text={'Asesoramiento para iniciar la empresa'} />
          <Step color={'violet'} icon={faCogs} text={'Estrategias de desarrollo del emprendimiento'} />
          <Step color={'teal'} icon={faChartLine} text={'Acciones a tomar para el crecimiento de la empresa'} />
          <Step color={'indigo'} icon={faHourglassHalf} text={'Asegurar sustentabilidad y sostenibilidad a largo plazo'} />
        </ul>
      </Container>
    </RainbowJumbotron>
  )
}

export default HowToStart