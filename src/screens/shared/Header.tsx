import styled from 'styled-components';

const Header = styled.div`
  padding: 1em;
  text-align: center;
`
export default Header
