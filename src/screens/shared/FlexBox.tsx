import styled from 'styled-components';

interface IFlexBox {
  row?: boolean
}

const FlexBox = styled.div<IFlexBox>`
  display: flex;
  flex-direction: ${props => props.row ? 'row' : 'column'};
  flex: 1 1;
  flex-wrap: wrap;
  justify-content: center;
  max-width: 100%;
  max-height: 100%;
`
export default FlexBox