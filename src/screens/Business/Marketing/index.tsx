import Heading from "../../shared/Heading"
import Image from "../../shared/Image"
import Jumbotron from "../../shared/Jumbotron"
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel'
import Container from "../../shared/Container";
import styled from "styled-components";


const ThisJumbotron = styled(Jumbotron)`
  background-image: radial-gradient(30% 0.3em at 50% 150px, blue, transparent),
    radial-gradient(ellipse at bottom right,  white 65%, yellow);
`

const CenteredHeading = styled(Heading)`
 text-align: center;
`

const Marketing = () => (
  <ThisJumbotron>
    <Container>
      <CenteredHeading size={2}>Marketing</CenteredHeading>
    </Container>
  </ThisJumbotron>
)
export default Marketing