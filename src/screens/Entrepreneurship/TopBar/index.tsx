import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import Image from "../../shared/Image";
import NavBar from "./NavBar";
import NavHero from "./NavHero";
import NavItem from "./NavItem";


const HomeButton = styled.div`
  font-weight: 600;
  color: darkkhaki;
  transition: color 0.2s ease-out;
  font-size: 1.2em;
  display: inline-block;
  padding: 5px;

  &: hover {
    color: khaki;
    transition: 0.2s ease-out;
    box-shadow: inset 0 0 100px 100px rgba(255, 255, 255, 0.1);
  }
`

const QBOLogo = styled(Image)`
  height: 1.5em;
  margin: -0.4em 0;
`

const TopBar = () => {
  const [paintBackground, setPaintBackground] = useState(false);

  useEffect(() => {
    const handleScroll:any = ({target: {scrollTop}}:any) => {
      setPaintBackground(scrollTop > 200);
    };
    const root = document.querySelector('#root')
    if (root) root.addEventListener('scroll', handleScroll)
    return () => {if (root) root.removeEventListener('scroll', handleScroll)};
  }, [])

  return (
  <NavBar paintBackground={paintBackground}>
    <NavItem>
      <NavHero>
        <Link to='/' style={{ textDecoration: 'unset' }}>
          <HomeButton>
            <QBOLogo src='/images/qboit_logo.png'/>
            QBOit
          </HomeButton>
        </Link> Entrepreneur Consulting
      </NavHero>
    </NavItem>
  </NavBar>
)}
export default TopBar