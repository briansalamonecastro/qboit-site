import styled from "styled-components"

const Credits = styled.p`
  position: absolute;
  bottom: 0px;
  right: 15px;
  font-size: 0.5em;
`
export default Credits