import styled from "styled-components";

const ImageContainer = styled.div`
  width: 40%;
  float: left;
  margin-right: 1em;
`

export default ImageContainer
