import styled from "styled-components";

export const colorDark = '#0000005f';
export const colorBlack = '#000000af 90%';
export const colorWhite = '#ffffffaf 70%';

interface IFloatingText {
  x?: number;
  y?: number;
  fadeColor?:string;
}
const FloatingText = styled.div<IFloatingText>`
  position: absolute;
  width: 300px;
  padding-bottom: 2em;
  bottom: ${props => props.y}%;
  left: ${props => props.x}%;
  background-image: linear-gradient(180deg,${props=> props.fadeColor || colorDark} , transparent);
  transform: translate(-${props => props.x}%, ${props => props.y}%);
`
export default FloatingText;
