import React from "react";
import Page from "../shared/Page";
import Banner from "./Banner";
import TopBar from "./TopBar";
import ContactUs from "../../components/ContactUs";
import Footer from "../../components/Footer";
import Networking from "./Networking";
import Marketing from "./Marketing";


const Business = () => (
  <Page>
    <TopBar />
    <Banner />
    <Networking />
    {/* <Marketing /> */}
    <ContactUs />
    <Footer />
  </Page>
)

export default Business