import React from "react";
import styled from "styled-components";
import Jumbotron from "../../screens/shared/Jumbotron";

const ColoredJumbotron = styled(Jumbotron)`
  background-color: #1b1449;
  min-height: 20vh;
`
const P = styled.p`
  font-size: 0.8em;
  color: #5e5e5e;
  text-align: left;
`

const Footer = () => {

  return (
    <ColoredJumbotron>
      <P>
        © 2021 QBOit. Todos los derechos reservados<br/>
        Creada por Brian Salamone
      </P>
    </ColoredJumbotron>
  )
}

export default Footer
