import React from "react";
import Page from "../shared/Page";
import Banner from "./Banner";
import HowToStart from "./HowToStart";
import TopBar from "./TopBar";
import SuccessfulCases from "./SuccessfulCases";
import WhyUs from "./WhyUs";
import ContactUs from "../../components/ContactUs";
import Footer from "../../components/Footer";


const Entrepreneurship = () => (
  <Page>
    <TopBar />
    <Banner />
    <WhyUs />
    <HowToStart />
    {/* <SuccessfulCases /> */}
    <ContactUs />
    <Footer />
  </Page>
)

export default Entrepreneurship