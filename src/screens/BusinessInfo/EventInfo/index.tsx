import styled from "styled-components"
import Heading from "../../shared/Heading"
import Return from "../Return";

const Div = styled.div`
  width: 800px;
  max-width: 100%;
  margin: 0 auto;
  background-color: white;
  padding: 5em 2em;

  @media (max-width: 700px){
    width: 100%;
    padding: 0.5em;
    margin: 0;
    font-size: 0.9em;
  }
`
const EventInfo = () => (
  <Div>
    <Return />
    <br id='networking'/>
    <Heading size={2}>Networking</Heading>
    <p>
      Es una actividad socio económica que generamos entre empresarios, emprendedores y profesionales para que se concreten relaciones empresariales a través de crear redes de contactos para tomar, crear y desarrollar oportunidades de negocios, compartir información y buscar clientes potenciales. Puede desarrollarse además de en el comercio, la industria y la economía en general, en ámbitos profesionales, deportivos, espirituales y muchos otros.
    </p>
    <p>
      En el ejercicio de estas redes, se utilizan mercadotecnia relacional, trabajo en red, redes de mercadeo o redes de negocio.
    </p>
    <p>
      Somos una empresa que prestamos servicios de ayuda a los emprendedores para que consigan desde nuevos contactos hasta su propio desarrollo profesional.
    </p>
    <br id='foros'/>
    <Heading size={3}>Foros</Heading>
    <p>
      Los foros internacionales constituyen un mecanismo de acceso al intercambio de experiencias y conocimientos técnicos, que permiten y fomentan el desarrollo de mejores prácticas y estándares internacionales, en el ámbito de competencia de las autoridades reguladoras.
    </p>
    <br id='congresos'/>
    <Heading size={3}>Congresos</Heading>
    <p>
      Este tipo de encuentros es un sector muy importante en lo que se refiere a nivel de asistencia, al nivel de las ponencias profesionales, información y comunicación, generación de temas interesantes para las diferentes áreas de la Salud, la Economía, la Bioquímica, la Farmacéutica, la Industria, el Comercio, el Turismo, la Innovación, la Tecnología, la Educación, las Finanzas, etc.
    </p>
    <p>
      Se estructura con Presentaciones sectoriales de cada área de interés, con ponencias,  debates y conclusiones. Con presentación de nuevos productos y servicios, publicidad para empresas, sedctor público y privado, Bancos,  Universidades entre otros y generan un alto movimiento económico 
    </p>
    <p>
      En períodos de escaso movimiento aéreo y terrestre, se consolidó la metodología virtual de los sectores MICE (Meetings, Incentives, Conventions and Exhibitions). Acercando el mundo a cada participante, con menores costos pero igual alto nivel de difusión e interrelacionamiento.y manteniendo en su extensión de uno a 3 días de trabajo intenso y motivador.
    </p>
    <br id='turismo-de-reuniones'/>
    <Heading size={3}>Turismo de Reuniones</Heading>
    <p>
      Consiste en aquellas actividades realizadas por viajeros solos, con su familia, compañeros de trabajo, socios, etc., que viajan a alguna localidad dentro de su país o al extranjero, con el objetivo principal de participar en una reunión, actividad grupal, conferencia, Congresos, Convenciones, Ferias, Exposiciones, acciones de Incentivos, Conferencias, Rondas de Negocios, Misiones Comerciales y Eventos en general.
    </p>
    <p>
      Cabe destacar que Argentina es uno de los principales 15 destinos mundiales en el ranking internacional de Sedes para la realización de Congresos, Convenciones, Ferias, Incentivos y demás eventos internacionales, donde se entrecruzan el Turismo, el Desarrollo Económico y los intereses comerciales de exportación e importación de empresas y países, siendo líder en América Latina por la calidad y diversidad de su oferta turística y la infraestructura existente.
    </p>
    <br id='turismo-de-incentivos'/>
    <Heading size={3}>Turismo de Incentivos</Heading>
    <p>
      Es la realización de viajes y actividades programadas, como estrategia empresarial para estímulo del rendimiento laboral y motivación del personal, destacando a aquellos que demuestran liderazgo, mayor potencial dirigencial y capacidad de trabajo en equipo.
    </p>
    <br id='turismo-de-negocios'/>
    <Heading size={3}>Turismo de Negocios: Misiones Comerciales</Heading>
    <p>
      En estos viajes la motivación principal es la realización de negocios, importar, exportar, vender, comprar, dentro del MICE que engloba también Turismo de reuniones, Conferencias Congresos,  Exposiciones Rondas de Negocios y Misiones Comerciales directas.
    </p>
  </Div>
)

export default EventInfo