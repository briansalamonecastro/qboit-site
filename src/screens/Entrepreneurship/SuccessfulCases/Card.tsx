import React from 'react'
import styled from 'styled-components'

const StyledDiv = styled.div`
  background-color: white;
  box-shadow: 0 0.5em 0.5em #0000007f;
  border-radius: 1em;
  padding: 1em; 
  margin: 1em;
  flex: 1 1 18em;
  aspect-ratio: 9 / 5;
  overflow: auto;

  @media (max-width: 700px){
    margin-left: 0;
    margin-right: 0;
  }
`

const Card = ({ children }: any) => {
  return (
    <StyledDiv>
      {children}
    </StyledDiv>
  )
}

export default Card