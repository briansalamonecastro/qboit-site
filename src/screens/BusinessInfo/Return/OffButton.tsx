import styled from "styled-components";
import Button from "../../shared/Button";

const OffButton = styled(Button)`
  background-color: white;
  color: ${props=>props.color};
  border-color: ${props=>props.color};

  &: hover{
    transition: 0.2s ease-out;
    color: white;
    background-color: ${props=>props.color};
  }
`

export default OffButton