import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram, faFacebook, faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import LilianaSection from "./LilianaSection";
import Container from "../../screens/shared/Container";
import FlexBox from "../../screens/shared/FlexBox";
import Heading from "../../screens/shared/Heading";
import Jumbotron from "../../screens/shared/Jumbotron";
import Paragraph from "../../screens/shared/Paragraph";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";


const ColoredJumbotron = styled(Jumbotron)`
  background-image: linear-gradient(120deg, transparent 50%, #00b1b1 50.1%, #00b1b1),
                    linear-gradient(120deg, transparent 30%, cyan 30.1%, cyan);
  min-height: 70vh;
`

const Div = styled.div`
  flex: 1 1;
  margin-bottom: 2em;
`

const Li = styled.li`
  font-size: 1.2em;
  margin: 0.5em;
`
const ThisHeading = styled(Heading)`
  @media (max-width: 700px){
    font-size: 2em;
  }
`

const Ul = styled.ul`
  margin-left: 0;
`
const A = styled.a`
  margin-left: 0.5em;
`

const ContactUs = () => (
  <ColoredJumbotron id='contact-us'>
    <Container>
      <ThisHeading size={2}>Contactate con nosotros!</ThisHeading>
      <FlexBox row>
        <Div>
          <Paragraph>
            Despejamos tus dudas por cualquiera de nuestras redes:
          </Paragraph>
          <Ul className='fa-ul'>
            <Li>
              <FontAwesomeIcon icon={faEnvelope} fixedWidth size='lg' />
              <A href="mailto:info@qboit.com.ar">info@qboit.com.ar</A>
            </Li>
            <Li>
              <FontAwesomeIcon icon={faWhatsapp} fixedWidth size='lg' />
              <A href='https://web.whatsapp.com/send/?phone=+541160205873'>11 6020-5873</A>
            </Li>
            <Li>
              <FontAwesomeIcon icon={faInstagram} fixedWidth size='lg' />
              <A href="https://www.instagram.com/qboit_consultora/">@qboit_consultora</A>
            </Li>
            <Li>
              <FontAwesomeIcon icon={faFacebook} fixedWidth size='lg' />
              <A href="https://www.facebook.com/qboit/">facebook.com/qboit</A>
            </Li>
          </Ul>
        </Div>
        <LilianaSection />
      </FlexBox>
    </Container>
  </ColoredJumbotron>
)

export default ContactUs
