import styled from 'styled-components';

const Jumbotron = styled.div`
  min-height: 90vh;
  padding: 0.1px 0; /* This prevents child's margin from escaping the parent's bounds */
  display: flex;
  align-items: center;
  justify-content: center;
`
export default Jumbotron
