import styled from 'styled-components';

const Ul = styled.ul<any>`
  position: fixed;
  width: 100%;
  padding-left: 0em;
  z-index: 1;

  display: flex;
  flex-direction: row;
  list-style: none;
  
  margin: 0;
  background-color: ${props => props.paintBackground? '#17114d':'transparent'};
  border-bottom: 0.5em solid ${props => props.paintBackground? '#28169c':'transparent'};
  transition: background-color 0.5s, border-bottom 0.5s;
`
const NavBar = ({paintBackground, children}:any)=>(
  <Ul paintBackground={paintBackground}>{children}</Ul>
)
export default NavBar
