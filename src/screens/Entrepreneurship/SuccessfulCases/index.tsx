import React from "react";
import Jumbotron from "../../shared/Jumbotron";
import FlexBox from "../../shared/FlexBox";
import Heading from "../../shared/Heading";
import Paragraph from "../../shared/Paragraph";
import Image from "../../shared/Image";
import Card from "./Card";
import ImageContainer from "../../shared/ImageContainer";
import Container from "../../shared/Container";
import styled from "styled-components";

const jumbotronStyle = {
  backgroundImage: 'linear-gradient(10deg, gray 30%, transparent 30.1%, transparent), linear-gradient(-10deg, lightgray 70%, transparent 70.1%, transparent)',
}

const imageStyle = {
  width: '100%',
  borderRadius: '1em',
  aspectRatio: '1/1',
}

const paragraphStyle = {
  display: 'inline',
  fontSize: '1.2em'
}

const ThisContainer = styled(Container)`
  align-items: center;
`

const ThisHeading = styled(Heading)`
  margin-left: 1em;

  @media (max-width: 700px){
    margin-left: unset;
  }
`

const SuccessfulCases = () => {

  return (
    <Jumbotron style={jumbotronStyle}>
      <ThisContainer>
        <ThisHeading size={3}>Casos de Exito</ThisHeading>
        <FlexBox row>
          <Card>
            <ImageContainer>
              <Image style={imageStyle} src='/images/portrait.jpg' />
              <i>Pepito, CEO de Terepin</i>
            </ImageContainer>
            <Paragraph style={paragraphStyle}>
              Un caso de exito pero aca no en otro país lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum
            </Paragraph>
          </Card>
          <Card>
            <ImageContainer>
              <Image style={imageStyle} src='/images/portrait2.jpg' />
              <i>Julieta, Empresaria del carajo</i>
            </ImageContainer>
            <Paragraph style={paragraphStyle}>
              Otro caso de exito del espacio sideral
            </Paragraph>
          </Card>
        </FlexBox>
      </ThisContainer>
    </Jumbotron>
  )
}

export default SuccessfulCases