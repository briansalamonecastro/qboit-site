import styled from "styled-components"
import Button from "../../shared/Button"

interface IShinyButton{
  shinyColor:string;
}

const ShinyButton = styled(Button)<IShinyButton>`
  background-image: linear-gradient(90deg, transparent, #ffffffaf, #ffffff0f, transparent, cyan, transparent);
  background-size: 100% 10em;
  background-position: -25em;
  background-repeat: no-repeat;

  &: hover {
    transition: 0.5s ease-out;
    background-position: 25em;
    background-color: ${props => props.shinyColor};
  }
`
export default ShinyButton