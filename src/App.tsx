import React from 'react';
import { HashRouter as BrowserRouter, Route, Switch } from "react-router-dom";
import Portal from './screens/Portal';
import Entrepreneurship from './screens/Entrepreneurship';
import Business from './screens/Business';
import BusinessInfo from './screens/BusinessInfo';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={Portal}/>
        <Route path='/emprendedurismo' component={Entrepreneurship} />
        <Route exact path='/negocios' component={Business} />
        <Route path='/negocios/info' component={BusinessInfo} />
        <Route path='/it' component={() => { 
            window.location.replace('http://it.qboit.com.ar'); 
            return null;
        }}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
