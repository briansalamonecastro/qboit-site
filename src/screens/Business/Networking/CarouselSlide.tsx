import styled from "styled-components"

const CarouselSlide = styled.div`
  height: 70vh;
  color: white;
  transition: 0.3s ease-in-out;

  & img {
    height: 100%;
    object-fit: cover;
  }
  
  &: hover{
    cursor:pointer;
    filter: brightness(1.2);
  }
`
export default CarouselSlide