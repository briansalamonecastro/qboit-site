import React from 'react'
import styled from 'styled-components'
import Heading from '../../shared/Heading'
import Paragraph from '../../shared/Paragraph'
import FlexBox from '../../shared/FlexBox'
import Jumbotron from '../../shared/Jumbotron'
import ShinyButton from './ShinyButton'
import Credits from '../../Portal/Credits'

const BannerFlexBox = styled(FlexBox)`
  flex: 1 1;
  padding: 20px 50px;

  @media (max-width: 700px){
    margin: 1em;
    margin-top: 2em;
    background-image: linear-gradient(0deg, white, transparent 70%);
    padding: 1em;
  }
`

const BannerParagraph = styled(Paragraph)`
  border-radius: 2em;
  margin: 0.5em;
`

const BannerJumbotron = styled(Jumbotron)`
  position:relative;
  background-image: 
    linear-gradient(109.6deg, rgba(61,245,167,0) 35%,  #ebebeb 60.2%, #7ec5ff 91.1% ),
    url(/images/startup-women.jpg);
  background-position: left;
  background-size: cover, contain;
  background-repeat: no-repeat;

  @media (max-width: 1000px){
    background-position: left, -5em -8em;
    background-size: cover, cover;
  }
  @media (max-width: 700px){
    background-position: left, -12em;
    background-size: cover, cover;
  }
`

const Banner = () => {

  const goToContactUs = () => {
    const contactUs = document.getElementById('contact-us');
    if (contactUs) contactUs.scrollIntoView({ behavior: 'smooth' })
  };

  return (
    <BannerJumbotron>
      <FlexBox row>
        <div style={{ flex: '1 1 30%' }}></div>
        <BannerFlexBox>
          <Heading size={2}>
            ¿Querés emprender?
          </Heading>
          <BannerParagraph>
            Nosotros te guiamos y acompañamos en todo el proceso para promover el despegue y desarrollo de tu empresa. Primera entrevista para evaluación y análisis sin cargo.
            <br />
            <ShinyButton color={'#17114d'} shinyColor={'#2f19b9'} size={1} onClick={goToContactUs}>Consultanos acá</ShinyButton>
          </BannerParagraph>
        </BannerFlexBox>
      </FlexBox>
      <Credits>
        Photo by <a href="https://unsplash.com/@brookecagle?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Brooke Cagle</a> on <a href="https://unsplash.com/s/photos/woman-phone?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
      </Credits>
    </BannerJumbotron>)
}

export default Banner;