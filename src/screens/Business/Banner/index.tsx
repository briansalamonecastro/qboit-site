import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import OffButton from '../../BusinessInfo/Return/OffButton'
import ShinyButton from '../../Entrepreneurship/Banner/ShinyButton'
import Credits from '../../Portal/Credits'
import FlexBox from '../../shared/FlexBox'
import Heading from '../../shared/Heading'
import Jumbotron from '../../shared/Jumbotron'
import Paragraph from '../../shared/Paragraph'

const BannerFlexBox = styled(FlexBox)`
  flex: 1 1;
  padding: 20px 50px;
  background-image: radial-gradient(circle at center, white, transparent);
  text-align: center;

  @media (max-width: 700px){
    margin: 2em;
    padding: 1em;
    }
  }
`

const BannerDiv = styled.div`
  border-radius: 2em;
  width: 60%;
  margin: 0 auto;

  @media (max-width: 700px){
    width: 90%;
  }
`

const BannerJumbotron = styled(Jumbotron)`
  position: relative;
  background-image: 
    url(/images/business.jpg);
  background-position: left;
  background-size: cover, contain;
  background-repeat: no-repeat;

  @media (max-width: 700px){
    background-position: left, -8em;
    background-size: cover, cover;
  }
`
const CenteredDiv = styled.div`
  display: flex;
  justify-content: center;
  padding: 1em;
  @media (max-width: 700px){
    font-size: 0.8em;
  }
`

const Banner = () => {

  const goToRegister = () => {
    window.open('https://bit.ly/2Xsay2V', '_blank');
  }

  return (
    <BannerJumbotron>
      <BannerFlexBox>
        <Heading size={2}>
          Explorá tu entorno.
        </Heading>
        <BannerDiv>
          <Paragraph>
          Tenemos las herramientas de crecimiento y desarrollo para tu empresa. Conocé a tus potenciales clientes y proveedores.
          </Paragraph>
          <CenteredDiv>
            <Link to='/negocios/info'>
              <OffButton size={1} color={'#793702'}>Más info</OffButton>
            </Link>
            <ShinyButton onClick={goToRegister} size={1} color={'#793702'} shinyColor={'#a86229'}>
              Veni al próximo evento
            </ShinyButton>
          </CenteredDiv>
        </BannerDiv>
      </BannerFlexBox>
      <Credits>
        Photo by <a href="https://unsplash.com/@charles_forerunner?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Charles Forerunner</a> on <a href="https://unsplash.com/s/photos/business?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
      </Credits>
    </BannerJumbotron>)
}

export default Banner;