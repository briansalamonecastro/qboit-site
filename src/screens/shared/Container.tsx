import styled from "styled-components";


const Container = styled.div`
  max-height: 100%;
  max-width: 100%;
  margin: 3em;

  @media (max-width: 700px){
    margin: 1em;
  }
`
export default Container
