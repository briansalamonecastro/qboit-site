import styled from 'styled-components';

const Image = styled.img.attrs(props => ({
  src: props.src
}))`
  min-width: 0;
  min-height: 0;
  object-fit: cover; /*magic*/
`
export default Image