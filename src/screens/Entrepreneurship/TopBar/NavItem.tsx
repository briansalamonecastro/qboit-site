import styled from 'styled-components';

const NavItem = styled.li`
  padding: 0.5em 1em;
`
export default NavItem
