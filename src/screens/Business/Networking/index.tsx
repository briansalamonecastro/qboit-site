import Heading from "../../shared/Heading"
import Image from "../../shared/Image"
import Jumbotron from "../../shared/Jumbotron"
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel'
import Container from "../../shared/Container";
import styled from "styled-components";
import FlexBox from "../../shared/FlexBox";
import CarouselSlide from "./CarouselSlide";
import FloatingText, { colorBlack, colorWhite } from "./FloatingText";
import { HashLink } from 'react-router-hash-link';

const ThisJumbotron = styled(Jumbotron)`
  background-image: radial-gradient(30% 0.3em at 50% 150px, crimson, transparent),
      radial-gradient(ellipse at top left,  white 65%, yellow);

  @media (max-width: 700px){
    font-size: 0.8em;
    background-image: radial-gradient(30% 0.3em at 50% 200px, crimson, transparent),
      radial-gradient(ellipse at top left,  white 65%, yellow);
  }
`

const ThisFlexBox = styled(FlexBox)`
  justify-content: center;
  text-align:center;

  & .carousel-root{
    margin: auto;
    max-width: 700px;
  }
`

const CenteredHeading = styled(Heading)`
 text-align: center;
`

const P = styled.p`
  font-size: 1.2em;
`

const Networking = () => {
  return (
    <ThisJumbotron>
      <Container>
        <ThisFlexBox>
          <CenteredHeading size={2}>Nuestros eventos de networking</CenteredHeading>
          <P>
            Llevamos más de 10 años organizando eventos de distintas índoles para <br />
            promocionar a nuestros asociados. Sumate y cosechá sus beneficios!
          </P>
          <Carousel showArrows infiniteLoop autoPlay showThumbs={false} stopOnHover={false} showStatus={false}>
            <HashLink to='/negocios/info#networking'>
              <CarouselSlide>
                <Image src='/images/networking.jpg' />
                <FloatingText x={50} y={35} style={{ fontSize: '0.8em' }}>
                  <Heading size={3}>Rondas de negocios</Heading>
                  Conocé a tus potenciales clientes y proveedores.
                </FloatingText>
              </CarouselSlide>
            </HashLink>
            <HashLink to='/negocios/info#foros'>
              <CarouselSlide>
                <Image src='/images/foros.png' style={{ objectFit: 'contain', paddingRight: '20%' }} />
                <FloatingText x={100} y={20} fadeColor={colorBlack}>
                  <Heading size={3}>Foros</Heading>
                  Enterate de las últimas novedades de tu rubro o sector.
                </FloatingText>
              </CarouselSlide>
            </HashLink>
            <HashLink to='/negocios/info#congresos'>
              <CarouselSlide>
                <Image src='/images/congresos.png'
                  style={{ objectFit: 'contain', paddingLeft: '15%' }} />
                <FloatingText x={5} y={90} fadeColor={colorBlack} style={{ width: '200px' }}>
                  <Heading size={3}>Congresos</Heading>
                  Ampliá tus horizontes profesionales y comerciales.
                </FloatingText>
              </CarouselSlide>
            </HashLink>
            <HashLink to='/negocios/info#turismo-de-reuniones'>
              <CarouselSlide>
                <Image src='/images/turismo-de-reuniones.png' />
                <FloatingText x={50} y={40} fadeColor={colorBlack}>
                  <Heading size={3}>Turismo de reuniones</Heading>
                  Viajá, hacé contactos y disfrutar nuevas locaciones.
                </FloatingText>
              </CarouselSlide>
            </HashLink>
            <HashLink to='/negocios/info#turismo-de-incentivos'>
              <CarouselSlide>
                <Image src='/images/turismo-de-incentivos.png' />
                <FloatingText x={100} y={100} fadeColor={colorWhite} style={{ color: 'black' }}>
                  <Heading size={3}>Turismo de incentivos</Heading>
                  Motivá e incentivá a tus empleados más capaces.
                </FloatingText>
              </CarouselSlide>
            </HashLink>
            <HashLink to='/negocios/info#turismo-de-negocios'>
              <CarouselSlide>
                <Image src='/images/turismo-de-negocios.jpg' />
                <FloatingText x={10} y={20}>
                  <Heading size={3}>Misiones comerciales</Heading>
                  Internacionalizá tu empresa.
                </FloatingText>
              </CarouselSlide>
            </HashLink>
          </Carousel>
        </ThisFlexBox>
      </Container>
    </ThisJumbotron>
  )
}
export default Networking